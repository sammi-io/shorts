package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/sammi-io/pants/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		log.WithError(err).Error("error running command")
		os.Exit(1)
	}
}
