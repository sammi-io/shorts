package shorts

import (
	"github.com/sirupsen/logrus"
	"go.starlark.net/starlark"
)

// Workspace a Shorts workspace
type Workspace struct {
	File  string
	Funcs starlark.StringDict
}

// NewWorkspace create a new workspace
func NewWorkspace() *Workspace {
	funcs := starlark.StringDict{
		"workspace": starlark.NewBuiltin("workspace", workspaceFn),
	}
	return &Workspace{
		File:  WorkspaceName,
		Funcs: funcs,
	}
}

// Exec execute starlark workspace file
// should be used for fetching deps and any
// other workspace initialization
func (ws *Workspace) Exec() error {
	thread := new(starlark.Thread)
	_, err := starlark.ExecFile(thread, WorkspaceName, nil, ws.Funcs)
	return err
}

func workspaceFn(th *starlark.Thread, _ *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var name string
	if err := starlark.UnpackArgs("workspace", args, kwargs, "name", &name); err != nil {
		return nil, err
	}

	logrus.Infof("initializing workspace '%s'", name)

	return starlark.None, nil
}
