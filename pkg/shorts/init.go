package shorts

import (
	"fmt"
	"os"
)

// Initialize initialize shorts environment
func Initialize() error {
	if _, err := os.Stat(WorkspaceName); os.IsNotExist(err) {
		return createWorkspaceFile()
	}
	return fmt.Errorf("%s already exists", WorkspaceName)
}

func createWorkspaceFile() error {
	_, err := os.Create(WorkspaceName)
	return err
}
