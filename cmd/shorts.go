package cmd

import (
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "shorts",
	Short: "Shorts - a monorepo focused build tool",
	Run: func(cmd *cobra.Command, args []string) {

	},
}

// Execute run root shorts command
func Execute() error {
	return rootCmd.Execute()
}
