package cmd

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/sammi-io/pants/pkg/shorts"
)

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "initialize a Short environment",
	Run: func(cmd *cobra.Command, args []string) {
		if err := shorts.Initialize(); err != nil {
			logrus.WithError(err).Error("failed to intialize workspace")
			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
}
