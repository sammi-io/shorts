package cmd

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/sammi-io/pants/pkg/shorts"
)

var depsCmd = &cobra.Command{
	Use:   "deps",
	Short: "pull dependencies specified in your workspace",
	Run: func(cmd *cobra.Command, args []string) {
		ws := shorts.NewWorkspace()
		if err := ws.Exec(); err != nil {
			logrus.WithError(err).Error("error in pulling workspace deps")
			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(depsCmd)
}
