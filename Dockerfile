FROM golang:1.11.5 AS builder
WORKDIR /opt/src/shorts
RUN mkdir -p /opt/bin
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o dist/shorts ./main.go && \
    cp dist/shorts /opt/bin
ENTRYPOINT [ "/opt/bin/shorts" ]
