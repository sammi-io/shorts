module gitlab.com/sammi-io/pants

require (
	github.com/coreos/etcd v3.3.11+incompatible // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/afero v1.2.1 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/ugorji/go/codec v0.0.0-20190128213124-ee1426cffec0 // indirect
	go.starlark.net v0.0.0-20190131184301-c1a3d543d128
	golang.org/x/crypto v0.0.0-20190131182504-b8fe1690c613 // indirect
	golang.org/x/sys v0.0.0-20190201152629-afcc84fd7533 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
