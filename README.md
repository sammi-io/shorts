# Shorts

Shorts is a mono-repo focused, multi-language, build tool based on Pants (https://www.pantsbuild.org/) and Bazel (https://bazel.build).